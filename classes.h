#pragma once

#include <string>
#include <memory>
#include <exception>
#include <spawn.h>
#include <string.h>
#include <curl/curl.h>
#include <sqlite3.h>

#define LException(x) Exception(x, __FILE__, __LINE__)
class Exception : public std::exception {
public:
    Exception(const char* msg, const char* file, int line) : _msg(msg) {
        add_line_info(file, line);
    }
    Exception(const std::string& msg, const char* file, int line) : _msg(std::move(msg)) {
        add_line_info(file, line);
    }
    const char* what() const noexcept {
        return this->_msg.c_str();
    }

private:
    inline void add_line_info(const char* file, int line) {
        this->_msg += " on ";
        this->_msg += file;
        this->_msg += ':';
        this->_msg += std::to_string(line);
    }
    std::string _msg;
};

class Lock {
public:
    Lock(const std::string& lock_path);
    ~Lock();

private:
    int _lockfd = -1;
    std::string _lock_path;
};

class Database {
public:
    // https://stackoverflow.com/a/2173764
    Database(const Database&) = delete;
    Database& operator=(const Database&) = delete;

    Database(const std::string& path);
    ~Database();
    constexpr sqlite3* ptr() {
        return this->_ptr;
    }

private:
    sqlite3* _ptr = nullptr;
};

class SqliteStmt {
public:
    // https://stackoverflow.com/a/2173764
    SqliteStmt(const SqliteStmt&) = delete;
    SqliteStmt& operator=(const SqliteStmt&) = delete;

    SqliteStmt() = default;
    ~SqliteStmt() {
        sqlite3_finalize(this->_ptr);
    }
    void prepare(Database& db, const char* sql) {
        int sqlite_code;
        sqlite3_finalize(this->_ptr);
        sqlite_code = sqlite3_prepare_v2(
            db.ptr(),
            sql,
            -1,
            &this->_ptr,
            nullptr);
        if (sqlite_code != SQLITE_OK) {
            throw LException("Failed to prepare statement: " + std::string(sqlite3_errstr(sqlite_code)));
        }
    }
    void finalize() {
        sqlite3_finalize(this->_ptr);
        this->_ptr = nullptr;
    }
    constexpr sqlite3_stmt* ptr() {
        return this->_ptr;
    }

private:
    sqlite3_stmt* _ptr = nullptr;
};

class Curl {
public:
    Curl() {
        CURL* curl_ptr = curl_easy_init();
        if (!curl_ptr) {
            throw LException("Failed to create curl easy session");
        }
        this->_ptr.reset(curl_ptr, curl_easy_cleanup);
    }
    CURL* ptr() {
        return this->_ptr.get();
    }

private:
    std::shared_ptr<CURL> _ptr;
};

class CurlSlist {
public:
    // https://stackoverflow.com/a/2173764
    CurlSlist(const CurlSlist&) = delete;
    CurlSlist& operator=(const CurlSlist&) = delete;

    CurlSlist() = default;
    ~CurlSlist() {
        curl_slist_free_all(this->_ptr);
    }
    void append(const char* str) {
        struct curl_slist* tmp = curl_slist_append(this->_ptr, str);
        if (!tmp) {
            throw LException("Failed to create new slist");
        }
        this->_ptr = tmp;
    }
    constexpr struct curl_slist* ptr() {
        return this->_ptr;
    }

private:
    struct curl_slist* _ptr = nullptr;
};

class FileActions {
public:
    // https://stackoverflow.com/a/2173764
    FileActions(const FileActions&) = delete;
    FileActions& operator=(const FileActions&) = delete;

    FileActions();
    ~FileActions();
    void addclose(int fd);
    void adddup2(int fd, int newfd);

    posix_spawn_file_actions_t actions;
};

class Pipe {
public:
    // https://stackoverflow.com/a/2173764
    Pipe(const Pipe&) = delete;
    Pipe& operator=(const Pipe&) = delete;

    Pipe();
    ~Pipe();
    void close_read();
    void close_write();

    int read = -1;
    int write = -1;
};
