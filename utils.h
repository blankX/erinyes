#pragma once

#include <string>
#include <vector>
#include <optional>
#include <ctime>
#include "classes.h"

constexpr size_t MAX_FILE_SIZE = 10 * 1024 * 1024;

typedef struct UrlData {
    std::time_t last_pull = -1;
    std::optional<std::string> last_modified;
    std::optional<std::string> etag;
    std::optional<std::vector<char>> previous_content;
    std::optional<std::string> previous_content_type;
    std::vector<char> content;
    std::optional<std::string> content_type;
    std::optional<std::string> redirect_url;
} UrlData;

enum ResponseType {
    Unknown,
    Ok,
    Redirect,
    NotModified
};

typedef struct CurlHeaderData {
    std::optional<std::string> content_type;
    std::optional<std::string> etag;
    std::optional<std::string> last_modified;
    ResponseType response_type = ResponseType::Unknown;
    Curl curl;
} CurlHeaderData;

std::string get_data_path();
void row_to_urldata(UrlData& url_data, Database& db, SqliteStmt& stmt);
void load_last_modified_and_etag(const UrlData& url_data, CurlSlist& headers);
extern "C" void sqlite_error_cb(void* ignored, int error_code, const char* error_message);
extern "C" size_t curl_write_cb(char* buf, size_t size, size_t nmemb, void* userp);
extern "C" size_t curl_header_cb(char* buf, size_t size, size_t nitems, void* userp);
void show_feed(const UrlData& url_data, const std::string& url);
