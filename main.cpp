#include <string>
#include <chrono>
#include <cstdio>
#include <cstdlib>
#include <curl/curl.h>
#include <sqlite3.h>
#include "utils.h"
#include "classes.h"

int wrapped_main(const char* url, const std::string& data_path) {
    UrlData url_data;
    std::vector<char> new_content;
    CurlHeaderData new_headers;
    char* new_redirect_url = nullptr;
    int sqlite_code;
    CURLcode curl_code;

    Curl curl;
    CurlSlist headers;
    Database db(data_path + "db");
    SqliteStmt stmt;

    new_headers.curl = curl;
    curl_easy_setopt(curl.ptr(), CURLOPT_VERBOSE, 1);
    curl_easy_setopt(curl.ptr(), CURLOPT_TIMEOUT_MS, 60000);
    curl_easy_setopt(curl.ptr(), CURLOPT_MAXFILESIZE, MAX_FILE_SIZE);
    curl_easy_setopt(curl.ptr(), CURLOPT_WRITEFUNCTION, curl_write_cb);
    curl_easy_setopt(curl.ptr(), CURLOPT_WRITEDATA, &new_content);
    curl_easy_setopt(curl.ptr(), CURLOPT_HEADERFUNCTION, curl_header_cb);
    curl_easy_setopt(curl.ptr(), CURLOPT_HEADERDATA, &new_headers);
    curl_code = curl_easy_setopt(curl.ptr(), CURLOPT_ACCEPT_ENCODING, "");
    if (curl_code != CURLE_OK) {
        throw LException("Failed to set CURLOPT_ACCEPT_ENCODING: " + std::string(curl_easy_strerror(curl_code)));
    }
    curl_code = curl_easy_setopt(curl.ptr(), CURLOPT_URL, url);
    if (curl_code != CURLE_OK) {
        throw LException("Failed to set CURLOPT_URL: " + std::string(curl_easy_strerror(curl_code)));
    }
    stmt.prepare(
        db,
        "CREATE TABLE IF NOT EXISTS pages (\n"
        "    url TEXT NOT NULL PRIMARY KEY,\n"
        "    last_pull INTEGER NOT NULL,\n"
        "    last_modified TEXT,\n"
        "    etag TEXT,\n"
        "    previous_content BLOB,\n"
        "    previous_content_type TEXT,\n"
        "    content BLOB NOT NULL,\n"
        "    content_Type TEXT,\n"
        "    redirect_url TEXT\n"
        ") STRICT;");
    sqlite_code = sqlite3_step(stmt.ptr());
    stmt.finalize();
    if (sqlite_code != SQLITE_DONE) {
        throw LException("Failed to run CREATE TABLE: " + std::string(sqlite3_errstr(sqlite_code)));
    }

    stmt.prepare(
        db,
        "SELECT last_pull, last_modified, etag, previous_content, "
        "previous_content_type, content, content_type, redirect_url "
        "FROM pages WHERE url = ?;");
    sqlite_code = sqlite3_bind_text(stmt.ptr(), 1, url, -1, SQLITE_STATIC);
    if (sqlite_code != SQLITE_OK) {
        throw LException("Failed to bind url to SELECT statement: " + std::string(sqlite3_errstr(sqlite_code)));
    }
    sqlite_code = sqlite3_step(stmt.ptr());
    if (sqlite_code == SQLITE_ROW) {
        row_to_urldata(url_data, db, stmt);
    } else if (sqlite_code != SQLITE_DONE) {
        throw LException("Failed to run SELECT: " + std::string(sqlite3_errstr(sqlite_code)));
    }
    stmt.finalize();

    load_last_modified_and_etag(url_data, headers);
    curl_code = curl_easy_setopt(curl.ptr(), CURLOPT_HTTPHEADER, headers.ptr());
    if (curl_code != CURLE_OK) {
        throw LException("Failed to set CURLOPT_HTTPHEADER: " + std::string(curl_easy_strerror(curl_code)));
    }
    curl_code = curl_easy_perform(curl.ptr());
    if (curl_code != CURLE_OK) {
        if (curl_code != CURLE_WRITE_ERROR || new_headers.response_type != ResponseType::Unknown) {
            throw LException("Failed to fetch page: " + std::string(curl_easy_strerror(curl_code)));
        }
        return 1;
    }
    if (new_headers.response_type == ResponseType::Ok &&
        new_headers.content_type == url_data.content_type &&
        new_content == url_data.content) {
        // server does not support 304
        new_headers.response_type = ResponseType::NotModified;
    }
    switch (new_headers.response_type) {
        case ResponseType::Ok:
            // GOD
            using std::chrono::system_clock;
            url_data.last_pull = system_clock::to_time_t(system_clock::now());
            if (!url_data.content.empty()) {
                url_data.previous_content = std::move(url_data.content);
            }
            if (url_data.content_type) {
                url_data.previous_content_type = std::move(url_data.content_type);
            }
            url_data.content = std::move(new_content);
            url_data.content_type = std::move(new_headers.content_type);
            url_data.redirect_url = std::nullopt;
            break;
        case ResponseType::Redirect:
            curl_code = curl_easy_getinfo(curl.ptr(), CURLINFO_REDIRECT_URL, &new_redirect_url);
            if (curl_code != CURLE_OK) {
                throw LException("Failed to get redirect url: " + std::string(curl_easy_strerror(curl_code)));
            }
            if (new_redirect_url == nullptr) {
                throw LException("Received a redirect with no redirect url");
            }
            if (!url_data.redirect_url || *url_data.redirect_url != new_redirect_url) {
                using std::chrono::system_clock;
                url_data.last_pull = system_clock::to_time_t(system_clock::now());
                url_data.redirect_url = new_redirect_url;
            }
            break;
        case ResponseType::NotModified:
            url_data.redirect_url = std::nullopt;
            break;
        default:
            throw LException("Received an unknown response type");
    }
    show_feed(std::move(url_data), url);
    return 0;
}

int main(int argc, char* argv[]) {
    std::string data_path = get_data_path();
    int ret = 1, sqlite_code;
    CURLcode curl_code;
    try {
        Lock lock_handle(data_path + "lock");
    } catch (const Exception& ex) {
        fprintf(stderr, "%s\n", ex.what());
        return 1;
    }

    if (argc != 2) {
        fprintf(stderr, "Usage: %s <url>\n", argc ? argv[0] : "erinyes");
        return 1;
    }
    curl_code = curl_global_init(CURL_GLOBAL_ALL);
    if (curl_code != CURLE_OK) {
        fprintf(stderr, "Failed to initialize curl");
        // curl_global_init docs say "If this function returns non-zero, [...] you cannot use the other curl functions".
        // reading the source code as of 05-06-2022, curl_easy_strerror should work even if curl_global_init doesn't,
        // but it's not guaranteed. thus we call curl_easy_strerror later, so even if it dies later, at least we'll have
        // gotten the "Failed to initialzie curl" message out
        fprintf(stderr, ": %s\n", curl_easy_strerror(curl_code));
        return 1;
    }
    // not done in wrapped_main since it has to be done before sqlite is initialized
    sqlite_code = sqlite3_config(SQLITE_CONFIG_LOG, sqlite_error_cb, nullptr);
    if (sqlite_code != SQLITE_OK) {
        fprintf(stderr, "Failed to set sqlite error callback: %s\n", sqlite3_errstr(sqlite_code));
        goto cleanup;
    }
    sqlite_code = sqlite3_initialize();
    if (sqlite_code != SQLITE_OK) {
        fprintf(stderr, "Failed to initialize sqlite: %s\n", sqlite3_errstr(sqlite_code));
        goto cleanup;
    }
    try {
        ret = wrapped_main(argv[1], data_path);
    } catch (const Exception& ex) {
        fprintf(stderr, "%s\n", ex.what());
    }

cleanup:
    curl_global_cleanup();
    sqlite_code = sqlite3_shutdown();
    if (sqlite_code != SQLITE_OK && ret == 0) {
        fprintf(stderr, "Failed to shutdown sqlite: %s\n", sqlite3_errstr(sqlite_code));
        ret = 1;
    }
    return ret;
}
