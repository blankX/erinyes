#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <sqlite3.h>
#include "classes.h"

Lock::Lock(const std::string& lock_path) : _lock_path(std::move(lock_path)) {
    char lockpid_buf[64];
    std::string current_pid;
    ssize_t pid_read;

    this->_lockfd = open(this->_lock_path.c_str(), O_RDWR | O_CREAT | O_CLOEXEC, 0600);
    if (this->_lockfd < 0) {
        if (errno == ENOENT) {
            throw LException("Failed to open lock file: One or more folders in " + this->_lock_path + " does not exist");
        } else {
            throw LException("Failed to open lock file: " + std::string(strerror(errno)));
        }
    }

    if (lockf(this->_lockfd, F_TLOCK, 0)) {
        if (errno == EACCES || errno == EAGAIN) {
            pid_read = read(this->_lockfd, lockpid_buf, sizeof(lockpid_buf) / sizeof(char));
            if (pid_read >= 0) {
                throw LException("Failed to lock: a process (PID: " + std::string(lockpid_buf, pid_read) + ") is already running");
            } else {
                throw LException("Failed to read from lock file: " + std::string(strerror(errno)));
            }
        } else {
            throw LException("Failed to lock: " + std::string(strerror(errno)));
        }
    }
    current_pid = std::to_string(getpid());
    if (write(this->_lockfd, current_pid.data(), current_pid.size()) < 0) {
        throw LException("Failed to write to lock file: " + std::string(strerror(errno)));
    }
}

Lock::~Lock() {
    if (this->_lockfd >= 0) {
        if (close(this->_lockfd)) {
            perror("Failed to close lock file");
        }
        // meh
        unlink(this->_lock_path.c_str());
    }
}

Database::Database(const std::string& path) {
    int sqlite_code = sqlite3_open(path.c_str(), &this->_ptr);
    if (sqlite_code != SQLITE_OK) {
        throw LException("Failed to open database: " + std::string(sqlite3_errstr(sqlite_code)));
    }
}

Database::~Database() {
    int sqlite_code = sqlite3_close(this->_ptr);
    if (sqlite_code != SQLITE_OK) {
        fprintf(stderr, "Failed to close database: %s\n", sqlite3_errstr(sqlite_code));
    }
}

FileActions::FileActions() {
    int err = posix_spawn_file_actions_init(&this->actions);
    if (err) {
        throw LException("Failed to create file_actions: " + std::string(strerror(err)));
    }
}

FileActions::~FileActions() {
    errno = posix_spawn_file_actions_destroy(&this->actions);
    if (errno) {
        perror("Failed to destroy file_actions");
    }
}

void FileActions::addclose(int fd) {
    int err = posix_spawn_file_actions_addclose(&this->actions, fd);
    if (err) {
        throw LException("Failed to add close(" + std::to_string(fd) + ") to file_actions: "
                + std::string(strerror(err)));
    }
}

void FileActions::adddup2(int fd, int newfd) {
    int err = posix_spawn_file_actions_adddup2(&this->actions, fd, newfd);
    if (err) {
        throw LException("Failed to add dup2(" + std::to_string(fd) + ", " + std::to_string(newfd) +
                ") to file_actions: " + std::string(strerror(err)));
    }
}

Pipe::Pipe() {
    int sides[2];
    if (pipe(sides)) {
        throw LException("Failed to open pipe: " + std::string(strerror(errno)));
    }
    this->read = sides[0];
    this->write = sides[1];
}

Pipe::~Pipe() {
    if (this->read >= 0 && close(this->read)) {
        perror("Failed to close read pipe");
    }
    if (this->write >= 0 && close(this->write)) {
        perror("Failed to close write pipe");
    }
}

void Pipe::close_read() {
    int fd = this->read;
    this->read = -1;
    if (close(fd)) {
        throw LException("Failed to close read pipe: " + std::string(strerror(errno)));
    }
}

void Pipe::close_write() {
    int fd = this->write;
    this->write = -1;
    if (close(fd)) {
        throw LException("Failed to close write pipe: " + std::string(strerror(errno)));
    }
}
