#include <string>
#include <vector>
#include <optional>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <spawn.h>
#include <unistd.h>
#include <sys/wait.h>
#include <curl/curl.h>
#include <sqlite3.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include "utils.h"

std::string get_data_path() {
    const char* xdg_config_home = std::getenv("XDG_CONFIG_HOME");
    const char* home = std::getenv("HOME");
    if (xdg_config_home && xdg_config_home[0]) {
        return std::string(xdg_config_home) + "/erinyes/";
    } else if (home && home[0]) {
        return std::string(home) + "/.config/erinyes/";
    } else {
        throw LException("Failed to find config path: XDG_CONFIG_HOME and HOME are unset");
    }
}

void row_to_urldata(UrlData& url_data, Database& db, SqliteStmt& stmt) {
    const unsigned char* tmp_column_text;
    const void* tmp_column_blob;

    url_data.last_pull = sqlite3_column_int64(stmt.ptr(), 0);
    tmp_column_text = sqlite3_column_text(stmt.ptr(), 1);
    if (tmp_column_text) {
        // https://stackoverflow.com/a/17746816
        url_data.last_modified = std::string(reinterpret_cast<const char*>(tmp_column_text), sqlite3_column_bytes(stmt.ptr(), 1));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract last_modified: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_text = sqlite3_column_text(stmt.ptr(), 2);
    if (tmp_column_text) {
        url_data.etag = std::string(reinterpret_cast<const char*>(tmp_column_text), sqlite3_column_bytes(stmt.ptr(), 2));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract etag: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_blob = sqlite3_column_blob(stmt.ptr(), 3);
    if (tmp_column_blob) {
        url_data.previous_content = std::vector(
            reinterpret_cast<const char*>(tmp_column_blob),
            &(reinterpret_cast<const char*>(tmp_column_blob)[sqlite3_column_bytes(stmt.ptr(), 3)]));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract previous_content: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_text = sqlite3_column_text(stmt.ptr(), 4);
    if (tmp_column_text) {
        url_data.previous_content_type = std::string(reinterpret_cast<const char*>(tmp_column_text), sqlite3_column_bytes(stmt.ptr(), 4));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract previous_content_type: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_blob = sqlite3_column_blob(stmt.ptr(), 5);
    if (tmp_column_blob) {
        url_data.content = std::vector(
            reinterpret_cast<const char*>(tmp_column_blob),
            &(reinterpret_cast<const char*>(tmp_column_blob)[sqlite3_column_bytes(stmt.ptr(), 5)]));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract content: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_text = sqlite3_column_text(stmt.ptr(), 6);
    if (tmp_column_text) {
        url_data.content_type = std::string(reinterpret_cast<const char*>(tmp_column_text), sqlite3_column_bytes(stmt.ptr(), 6));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract content_type: " + std::string(sqlite3_errmsg(db.ptr())));
    }
    tmp_column_text = sqlite3_column_text(stmt.ptr(), 7);
    if (tmp_column_text) {
        url_data.redirect_url = std::string(reinterpret_cast<const char*>(tmp_column_text), sqlite3_column_bytes(stmt.ptr(), 7));
    } else if (sqlite3_errcode(db.ptr()) != SQLITE_OK) {
        throw LException("Failed to extract redirect_url: " + std::string(sqlite3_errmsg(db.ptr())));
    }
}

void load_last_modified_and_etag(const UrlData& url_data, CurlSlist& headers) {
    std::string header;

    if (url_data.last_modified) {
        header = "If-Modified-Since: " + *url_data.last_modified;
        headers.append(header.c_str());
    }
    if (url_data.etag) {
        header = "If-None-Match: " + *url_data.etag;
        headers.append(header.c_str());
    }
}

std::string sha256(const std::vector<char>& bytes) {
    unsigned char digest_out[32];
    std::string result;
    char hex[3];
    size_t i;

    // not ideal using openssl, but i have no idea how to write my own sha256 func, so here we are
    if (EVP_Digest(bytes.data(), bytes.size(), digest_out, nullptr, EVP_sha256(), nullptr)) {
        result.reserve(64);
        for (i = 0; 32 > i; i++) {
            snprintf(hex, 3, "%02x", digest_out[i]);
            result.append(hex, 2);
        }
        return result;
    } else {
        throw LException("Failed to hash: " + std::string(ERR_error_string(ERR_get_error(), nullptr)));
    }
}

extern "C" void sqlite_error_cb(void* ignored, int error_code, const char* error_message) {
    fprintf(stderr, "sqlite message (error code %d): %s\n", error_code, error_message);
    (void)ignored;
}

extern "C" size_t curl_write_cb(char* buf, size_t size, size_t nmemb, void* userp) {
    std::vector<char>* new_content = (std::vector<char>*)userp;
    if (nmemb + new_content->size() > MAX_FILE_SIZE) {
        fprintf(stderr, "Too many bytes received (%lu > %lu)\n", nmemb + new_content->size(), MAX_FILE_SIZE);
        return 0;
    }
    new_content->insert(new_content->end(), buf, &(buf[nmemb]));
    return size * nmemb;
}

// key must be lowercase because... deal with it
static std::optional<std::string> get_header_value(const char* header, size_t header_len, const char* key) {
    size_t offset, length;
    if (!header_len) {
        return std::nullopt;
    }
    for (offset = 0; key[offset]; offset++) {
        if (offset >= header_len || std::tolower(header[offset]) != key[offset]) {
            return std::nullopt;
        }
    }
    if (offset + 1 >= header_len || header[offset++] != ':') {
        return std::nullopt;
    }
    while (header_len > offset && header[offset] == ' ') {
        offset++;
    }
    length = header_len - offset;
    while (length && header[offset + length - 1] == ' ') {
        length--;
    }
    if (!length) {
        return std::nullopt;
    }
    return std::string(&(header[offset]), length);
}

extern "C" size_t curl_header_cb(char* buf, size_t size, size_t nitems, void* userp) {
    CurlHeaderData* header_data = static_cast<CurlHeaderData*>(userp);
    std::optional<std::string> value;
    size_t header_len = nitems;
    long response_code;
    CURLcode curl_code;

    if (header_data->response_type == ResponseType::Unknown) {
        curl_code = curl_easy_getinfo(header_data->curl.ptr(), CURLINFO_RESPONSE_CODE, &response_code);
        if (curl_code != CURLE_OK) {
            fprintf(stderr, "Failed to get response code: %s\n", curl_easy_strerror(curl_code));
            return 0;
        }
        switch (response_code) {
            case 200:
                header_data->response_type = ResponseType::Ok;
                break;
            case 204:
            case 205:
                fprintf(stderr, "Server shouldn't have responded with %ld\n", response_code);
                return 0;
            case 301:
            case 302:
            case 303:
            case 307:
            case 308:
                header_data->response_type = ResponseType::Redirect;
                break;
            case 304:
                header_data->response_type = ResponseType::NotModified;
                break;
            default:
                fprintf(stderr, "Server responded with %ld\n", response_code);
                return 0;
        }
    }
    if (header_len > 2 && buf[header_len - 2] == '\r' && buf[header_len - 1] == '\n') {
        header_len -= 2;
    }
    if ((value = get_header_value(buf, header_len, "etag"))) {
        header_data->etag = std::move(value);
    } else if ((value = get_header_value(buf, header_len, "last-modified"))) {
        header_data->last_modified = std::move(value);
    } else if ((value = get_header_value(buf, header_len, "content-type"))) {
        header_data->content_type = std::move(value);
    }
    return size * nitems;
}

const char* rfc822_day_map[7] = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
const char* rfc822_month_map[12] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

std::string rfc822_date(std::time_t time) {
    char buf[1024];
    std::tm* tm = std::gmtime(&time);

    if (tm) {
        snprintf(buf, 1024, "%3s, %02d %3s %04d %02d:%02d:%02d GMT",
            rfc822_day_map[tm->tm_wday], tm->tm_mday,
            rfc822_month_map[tm->tm_mon], tm->tm_year + 1900,
            tm->tm_hour, tm->tm_min, tm->tm_sec);
        return buf;
    } else {
        throw LException("Failed to call gmtime: " + std::string(strerror(errno)));
    }
}

static std::string escape_html(const std::string& text) {
    std::string result;
    size_t offset = 0, special_offset;

    while ((special_offset = text.find_first_of("<>&", offset)) != std::string::npos) {
        result.append(text, offset, special_offset - offset);
        result.append(text[special_offset] == '<'
            ? "&lt;"
            : text[special_offset] == '>'
            ? "&gt;"
            : "&amp;");
        offset = special_offset + 1;
    }
    if (text.size() > offset) {
        result.append(text, offset, std::string::npos);
    }
    return result;
}

std::string diff(const std::string& lh_name, const std::optional<std::vector<char>>& lh,
        const std::string& rh_name, const std::vector<char>& rh) {
    pid_t pid;
    int wstatus;
    const char* new_argv[] = {"diff", "-u", "--label", lh_name.c_str(), "--label", rh_name.c_str(), "/proc/self/fd/3", "/proc/self/fd/4", nullptr};

    std::string stdout;
    char tmp_stdout[4096];
    size_t total_written = 9;
    ssize_t written, have_read;
    FileActions file_actions;
    Pipe lh_pipe, rh_pipe, stdout_pipe;

    file_actions.addclose(0);
    file_actions.adddup2(stdout_pipe.write, 1);
    file_actions.adddup2(stdout_pipe.write, 2);
    file_actions.addclose(stdout_pipe.write);
    if (lh_pipe.read != 3) {
        file_actions.adddup2(lh_pipe.read, 3);
        file_actions.addclose(lh_pipe.read);
    }
    file_actions.addclose(lh_pipe.write);
    if (rh_pipe.read != 4) {
        file_actions.adddup2(rh_pipe.read, 4);
        file_actions.addclose(rh_pipe.read);
    }
    file_actions.addclose(rh_pipe.write);

    // beat it until it fucking works
    errno = posix_spawnp(&pid, "diff", &file_actions.actions, nullptr, const_cast<char* const*>(new_argv), environ);
    if (errno) {
        throw LException("Failed to spawn diff: " + std::string(strerror(errno)));
    }
    lh_pipe.close_read();
    rh_pipe.close_read();
    stdout_pipe.close_write();

    // write to left-hand pipe
    if (lh) {
        while (lh->size() > total_written) {
            written = write(lh_pipe.write, &(lh->at(total_written)), lh->size() - total_written);
            if (written < 0) {
                throw LException("Failed to write to left-hand pipe: " + std::string(strerror(errno)));
            }
            total_written += written;
        }
        total_written = 0;
    }
    lh_pipe.close_write();

    // write to right-hand pipe
    while (rh.size() > total_written) {
        written = write(rh_pipe.write, &(rh[total_written]), rh.size() - total_written);
        if (written < 0) {
            throw LException("Failed to write to right-hand pipe: " + std::string(strerror(errno)));
        }
        total_written += written;
    }
    rh_pipe.close_write();

    do {
        have_read = read(stdout_pipe.read, tmp_stdout, 4096);
        if (have_read < 0) {
            throw LException("Failed to read from stdout pipe: " + std::string(strerror(errno)));
        } else if (have_read) {
            stdout.append(tmp_stdout, have_read);
        }
    } while (have_read);

    if (waitpid(pid, &wstatus, 0) < 0) {
        throw LException("Failed to wait for child: " + std::string(strerror(errno)));
    }
    if (WIFEXITED(wstatus)) {
        if (WEXITSTATUS(wstatus) == 0 || WEXITSTATUS(wstatus) == 1) {
            // cool, everything (as far as we know) worked out
        } else {
            throw LException("diff failed: exited with " + std::to_string(WEXITSTATUS(wstatus)));
        }
    } else if (WIFSIGNALED(wstatus)) {
        throw LException("diff died: terminated with " + std::to_string(WTERMSIG(wstatus)));
    } else {
        throw LException("diff status unknown");
    }

    return stdout;
}

void show_feed(const UrlData& url_data, const std::string& url) {
    std::string title, description, guid, lh_name = "empty", rh_name;
    std::string date = rfc822_date(url_data.last_pull);
    std::optional<std::string> previous_sha256, current_sha256;

    guid = date + "_" + url + "_";
    if (url_data.redirect_url) {
        guid += "redir_" + *url_data.redirect_url;
        title = "Redirected to " + *url_data.redirect_url;
        description = title;
    } else {
        if (url_data.previous_content) {
            previous_sha256 = sha256(*url_data.previous_content);
        }
        current_sha256 = sha256(url_data.content);
        guid += (previous_sha256 ? *previous_sha256 : "") + "_" +
            (url_data.previous_content_type ? *url_data.previous_content_type : "") + "_" +
            *current_sha256 + "_" + (url_data.content_type ? *url_data.content_type : "");
        title = url_data.previous_content ? "Content updated" : "New content";
        if (previous_sha256) {
            lh_name = *previous_sha256;
        }
        if (url_data.previous_content_type) {
            lh_name += " (" + *url_data.previous_content_type + ')';
        }
        rh_name = *current_sha256;
        if (url_data.content_type) {
            rh_name += " (" + *url_data.content_type + ')';
        }
        description = diff(std::move(lh_name), url_data.previous_content,
                std::move(rh_name), url_data.content);
    }
    printf("<?xml version=\"1.0\"?>\n"
           "<rss version=\"2.0\">\n"
           "<channel>\n"
           "    <title>%s updates</title>\n"
           "    <link>%s</title>\n"
           "    <description>Page updates for %s</description>\n"
           "    <item>\n"
           "        <title>%s</title>\n"
           "        <description>%s</description>\n"
           "        <guid isPermaLink=\"false\">%s</guid>\n"
           "        <pubDate>%s</pubDate>\n"
           "    </item>\n"
           "</channel>\n"
           "</rss>\n",
           escape_html(url).c_str(),
           escape_html(url).c_str(),
           escape_html(url).c_str(),
           escape_html(std::move(title)).c_str(),
           escape_html(std::move(description)).c_str(),
           escape_html(std::move(guid)).c_str(),
           date.c_str());
}
